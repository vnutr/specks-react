import React, {Component} from 'react';

class SpeckCell extends React.Component {

	constructor(props) {
		super(props);
	}

	render() {
		return (
			<div id={ this.props.cell_id } onClick={ () => this.props.clickHandler(this.props.row, this.props.cell) } className="speck-cell">{ this.props.label }</div>
		);
	}

}

export default SpeckCell;