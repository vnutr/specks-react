import React, {Component} from 'react';
import SpeckCell from "./specks-cell";

class Specks extends React.Component {
	constructor(props) {
		super(props);

		this.handleClick = this.handleClick.bind(this);

		this.state = {
			cells: {},
			steps: 0,
			active: true,
			empty: {
				row: 4,
				cell: 4,
			}
		};

		for (let i = 1; i <= 4; i++) {
			i = parseInt(i);
			this.state.cells[i] = {};
			for (let j = 1; j <= 4; j++) {
				j = parseInt(j);
				this.state.cells[i][j] =
						(i == this.state.empty.row && j == this.state.empty.cell) ?
								false : ((i - 1) * 4) + j;
			}
		}
	}

	componentDidMount() {
		this.shuffle();
	}

	shuffle() {
		let old = this.state.cells,
			empty = this.state.empty;
		for (let i = 1; i <= 4; i++) {
			for (let j = 1; j <= 4; j++) {
				let _i = this.rand(1, 4),
					_j = this.rand(1, 4),
					tmp = old[i][j];
				if (tmp == false) {
					empty = {
						row: _i,
						cell: _j
					};
				}
				old[i][j] = old[_i][_j];
				old[_i][_j] = tmp;
			}
		}
		this.setState({
			cells: old,
			empty: empty,
		});
	}

	isEnd() {
		let index = 1;
		for (let i in this.state.cells) {
			for (let j in this.state.cells[i]) {
				if (i == 4 && j == 4) continue;

				if (this.state.cells[i][j] != index) {
					return false;
				}
				index++;
			}
		}
		return true;
	}

	rand(min, max) {
		return Math.floor(Math.random()*(max-min+1)+min);
	}

	getSiblings(row, cell) {
		let siblings = {
			up: false,
			right: false,
			bottom: false,
			left: false,
		};

		if (row - 1 > 0)
			siblings.up = {
				row: row - 1,
				cell: cell
			};

		if (cell + 1 <= 4)
			siblings.right ={
				row: row,
				cell: cell + 1
			};

		if (row + 1 <= 4)
			siblings.bottom = {
				row: row + 1,
				cell: cell
			};

		if (cell - 1 > 0)
			siblings.left = {
				row: row,
				cell: cell - 1
			};

		return siblings;
	}

	canMove(row, cell) {
		let siblings = this.getSiblings(row, cell);
		if (!this.state.active)
			return false;
		return  (siblings.up && JSON.stringify(siblings.up) == JSON.stringify(this.state.empty)) ||
				(siblings.right && JSON.stringify(siblings.right) == JSON.stringify(this.state.empty)) ||
				(siblings.bottom && JSON.stringify(siblings.bottom) == JSON.stringify(this.state.empty)) ||
				(siblings.left && JSON.stringify(siblings.left) == JSON.stringify(this.state.empty));
	}

	move(row, cell) {
		if (this.canMove(row, cell)) {
			let old = this.state.cells;
			let tmp = old[row][cell];

			old[row][cell] = false;
			old[this.state.empty.row][this.state.empty.cell] = tmp;
			this.setState({
				empty: {
					row: row,
					cell: cell,
				},
				cells: old,
				steps: this.state.steps + 1,
			});
			if (this.isEnd())
				this.setState({
					active: false,
				});
		}
	}

	handleClick(row, cell) {
		row = parseInt(row);
		cell = parseInt(cell);
		this.move(row, cell);
	}

	render() {
		let cells = [];

		for (let i in this.state.cells) {
			for (let j in this.state.cells[i]) {
				cells.push(<SpeckCell clickHandler={ this.handleClick } key={ this.state.cells[i][j] } row={ i } cell={ j }  cell_id={ `speck-cell-${ i }-${ j }` } label={ this.state.cells[i][j] } />);
			}
		}

		return (<div className={ "specks " + (this.state.active && "active") }>
					{ cells }
					{ !this.state.active && <h1 className="specks-results">Well Done. You have made { this.state.steps } steps.</h1> }
				</div>);
	}
}

export default Specks;