import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import Specks from './components/specks';

ReactDOM.render(
	<Specks />,
    document.getElementById('root')
);
