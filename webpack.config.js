var path = require('path');
var webpack = require('webpack');
module.exports = {
    entry: './src/app/index.js',
    output: {
        path: path.join(__dirname, 'dist/js'),
        filename: 'app.js',
        publicPath: '/dist/js',
    },
    module: {
        loaders: [
            {
                test: /.jsx?$/,
                loader: 'babel-loader',
                exclude: /node_modules/,
                query: {
                    presets: ['es2015', 'react']
                }
            }
        ]
    },
    plugins: [
        new webpack.optimize.UglifyJsPlugin({
            compress: { warnings: false }
        })
    ],
    watch: true,
    devtool: "#source-map",
};